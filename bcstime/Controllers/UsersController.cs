﻿using bcstime.Domain.Services;
using bcstime.Domain.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcstime.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _users;
        private readonly ILogger<UsersController> _logger;
        public UsersController(IUserService users, ILogger<UsersController> logger)
        {
            this._users = users;
            this._logger = logger;
        }

        [HttpGet]
        public IEnumerable<UserView> Get()
        {
            return _users.ReadAll().ToArray();
        }
    }
}
