﻿using bcstime.db.Entity;
using Microsoft.EntityFrameworkCore;

namespace bcstime.db
{
    public interface IBcsTimeDbContext
    {
        DbSet<User> Users { set; get; }
        DbSet<Role> Roles { set; get; }
        DbSet<RoleUser> RoleUsers { set; get; }
    }
}