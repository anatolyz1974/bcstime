﻿using bcstime.db.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User", "dbo");
            builder.HasKey(x => x.UserId);
            builder.Property(x => x.UserId).HasColumnName("user_id").ValueGeneratedOnAdd();
            builder.Property(x => x.UserName).HasColumnName("user_name");
        }
    }
}
