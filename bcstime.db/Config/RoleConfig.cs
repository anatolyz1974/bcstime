﻿using bcstime.db.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db.Config
{
    public class RoleConfig : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Role", "dbo");
            builder.HasKey(x => x.RoleId);
            builder.Property(x => x.RoleId).HasColumnName("role_id").ValueGeneratedOnAdd();
            builder.Property(x => x.RoleName).HasColumnName("role_name");
        }
    }
}
