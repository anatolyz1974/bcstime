﻿using bcstime.db.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db.Config
{
    public class RoleUserConfig : IEntityTypeConfiguration<RoleUser>
    {
        public void Configure(EntityTypeBuilder<RoleUser> builder)
        {
            builder.ToTable("RoleUser", "dbo");
            builder.HasKey(x => x.RoleUserId);
            builder.Property(x => x.RoleUserId).HasColumnName("role_user_id").ValueGeneratedOnAdd();
            builder.Property(x => x.UserId).HasColumnName("user_id");
            builder.Property(x => x.RoleId).HasColumnName("role_id");
        }
    }
}
