﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db.Entity
{
    public class RoleUser
    {
        public int RoleUserId { set; get; }
        public int UserId { set; get; }
        public int RoleId { set; get; }
    }
}
