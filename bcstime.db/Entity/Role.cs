﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db.Entity
{
    public class Role
    {
        public int RoleId { set; get; }
        public string RoleName { set; get; }
    }
}
