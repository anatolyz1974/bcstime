﻿using bcstime.db.Config;
using bcstime.db.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.db
{
    public class BcsTimeDbContext : DbContext, IBcsTimeDbContext
    {
        public DbSet<User> Users { set; get; }
        public DbSet<Role> Roles { set; get; }
        public DbSet<RoleUser> RoleUsers { set; get; }

        public BcsTimeDbContext() : base()
        {

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // User ID =rec_cd_main_reader;Password=12345678A;Server=localhost;Port=5432;Database=rec_cd;Pooling=true;
            if (!optionsBuilder.IsConfigured)
            {
                // var connectionString = "Server=database;Port=5432;Database=APP;User ID=app;Password=lNT4C5kRLzOAtyyZ5c51;Pooling=true;";
                // var connectionString = "Server=localhost;Port=5432;Database=rgd;User ID=rgd_owner;Password=123456;Pooling=true;";
                var connectionString = "Persist Security Info=False;Integrated Security=true;Initial Catalog=BcsTime;server=DESKTOP-EDGBN6P";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new RoleConfig());
            modelBuilder.ApplyConfiguration(new RoleUserConfig());
        }
    }
}
