﻿using bcstime.db;
using bcstime.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bcstime.Domain.Services
{
    public class UserService : IUserService
    {
        IBcsTimeDbContext db;
        
        public UserService(IBcsTimeDbContext db)
        {
            this.db = db;
        }
        
        public ICollection<UserView> ReadAll()
        {
            return db.Users.Select(x => new UserView { Name = x.UserName }).ToList();
        }
    }
}
