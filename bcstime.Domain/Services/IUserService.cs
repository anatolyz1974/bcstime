﻿using bcstime.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace bcstime.Domain.Services
{
    public interface IUserService
    {
        ICollection<UserView> ReadAll();
    }
}
